import Users from "../models/user.js";

export const addUser = async (req) => {
    try {
        const newUser = new Users({
            UserName: req.body.UserName,
            Email: req.body.Email,
            Password: req.body.Password,
            UserType: req.body.UserType,
            CreatedAt: new Date(),
            UpdatedAt: new Date(),
            IsActive: req.body.IsActive,
            Roles: req.body.Roles,
            Permissions: req.body.Permissions,
        });

        return await Users.create(newUser);
    } catch (error) {
        throw error;
    }
}

export const updateUser = async (req) => {
    try {
        const updateFields = {
            UserName: req.body.UserName,
            Email: req.body.Email,
            Password: req.body.Password,
            UserType: req.body.UserType,
            UpdatedAt: new Date(),
            IsActive: req.body.IsActive,
            Roles: req.body.Roles,
            Permissions: req.body.Permissions,
        };

        return await Users.findByIdAndUpdate(
            req.body._id,
            { $set: updateFields },
            { new: true }
        );
    } catch (error) {
        throw error;
    }
}

export const deleteUser = async (req) => {
    try {
        return await Users.findByIdAndUpdate(
            req.body._id,
            {
                $set: {
                    IsActive: false,
                    UpdatedAt: new Date()
                }
            },
            { new: true }
        );
    } catch (error) {
        throw error;
    }
}

export const getUserById = async (req) => {
    try {
        return await Users.findOne({
            _id: req.body._id,
            IsActive: true
        });
    } catch (error) {
        throw error;
    }
}

export const getAllUsers = async () => {
    try {
        return await Users.find({
            IsActive: true
        });
    } catch (error) {
        throw error;
    }
}