import { getAllUsers, addUser, updateUser, deleteUser, getUserById } from "../services/userService.js";

export const GetAllUsers = async (req, res) => {
    try {
        const response = await getAllUsers();
        res.json(response);
    } catch (error) {
        console.error(error, "GetAllUsers");
        res.status(500).json({ error: error.message });
    }
}

export const AddUser = async (req, res) => {
    try {
        const response = await addUser(req);
        res.json(response);
    } catch (error) {
        console.error(error, "AddUser");
        res.status(500).json({ error: error.message });
    }
}

export const DeleteUser = async (req, res) => {
    try {
        if (req.body != null) {
            const response = await deleteUser(req);
            res.json(response);
        } else {
            console.log("User not found.");
        }
    } catch (error) {
        console.error(error, "DeleteUser");
        res.status(500).json({ error: error.message });
    }
}

export const UpdateUser = async (req, res) => {
    try {
        const response = await updateUser(req);
        res.json(response);
    } catch (error) {
        console.error(error, "UpdateUser");
        res.status(500).json({ error: error.message });
    }
}

export const GetUserById = async (req, res) => {
    try {
        const response = await getUserById(req);
        res.json(response);
    } catch (error) {
        console.error(error, "UpdateUser");
        res.status(500).json({ error: error.message });
    }
}