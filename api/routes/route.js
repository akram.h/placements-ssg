import express from 'express';
import { AddUser, UpdateUser, DeleteUser, GetAllUsers, GetUserById } from '../controllers/usersController.js';

const router = express.Router();

//Users
router.post('/addUser', AddUser);
router.put('/updateUser', UpdateUser);
router.put('/deleteUer', DeleteUser);
router.get('/getAllUsers', GetAllUsers);
router.get('/getUserById', GetUserById);

export default router;