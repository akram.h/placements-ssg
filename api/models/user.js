import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
    UserName: {
        type: String,
        required: true,
    },
    Email: {
        type: String,
        required: true,
        unique: true,
    },
    Password: {
        type: String,
        required: true,
    },
    UserType: {
        type: Number,
        required: true,
    },
    CreatedAt: {
        type: Date,
        default: () => new Date().toLocaleString(),
    },
    UpdatedAt: {
        type: Date,
        default: () => new Date().toLocaleString(),
    },
    IsActive: {
        type: Boolean,
        default: true,
    },
    Roles: {
        type: [String],
        required: true,
    },
    Permissions: {
        type: [String],
        required: true,
    },
});

const Users = mongoose.model('Users', userSchema);

export default Users;