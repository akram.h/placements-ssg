import mongoose from "mongoose";
import PlacementCell from "./placementcell";
import PlacementStatistics from "./placementstatistics";
import Corporates from "./corporate";
import CalendarEvents from "./calendarevent";
import BatchDetails from "./batchdetails";

const collegeSchema = new mongoose.Schema({
    Name: {
        type: String,
        default: ''
    },
    ContactInformation: contactInformationSchema,
    Website: {
        type: String,
        default: ''
    },
    Streams: [
        {
            type: String,
            default: ''
        }
    ],
    CoursesOffered: [
        {
            type: String,
            default: ''
        }
    ],
    PlacementCell: PlacementCell,
    PlacementStatistics: PlacementStatistics,
    Corporates: [
        Corporates
    ],
    BatchDetails: [
        BatchDetails
    ],
    CalendarEvents: CalendarEvents
});

const contactInformationSchema = new mongoose.Schema({
    Address: {
        type: String,
        default: ''
    },
    PhoneNumber: {
        type: String,
        default: ''
    },
    EmailAddress: {
        type: String,
        default: ''
    }
});

const College = mongoose.model('Colleges', collegeSchema);

export default College;