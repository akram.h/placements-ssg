import mongoose from "mongoose";

const CalendarEvents = new mongoose.Schema({
    EventDetails: [eventDetailSchema],
});

const eventDetailSchema = new mongoose.Schema({
    EventName: {
        type: String,
        default: '',
    },
    EventDate: {
        type: Date,
        default: '',
    },
    EventTime: {
        type: String,
        default: '',
    },
    EventLocation: {
        type: String,
        default: '',
    },
    EventDescription: {
        type: String,
        default: '',
    },
});

export default CalendarEvents;