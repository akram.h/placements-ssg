import mongoose from 'mongoose';

const Reminders = new mongoose.Schema({
    UpcomingEvents: [{
        EventName: {
            type: String,
            default: ''
        },
        EventDate: {
            type: Date,
            default: ''
        },
        EventTime: {
            type: String,
            default: ''
        },
        EventLocation: {
            type: String,
            default: ''
        },
    }],
    ApplicationDeadlines: [{
        JobTitle: {
            type: String,
            default: ''
        },
        CompanyName: {
            type: String,
            default: ''
        },
        DeadlineDate: {
            type: Date,
            default: ''
        },
        ApplicationLink: {
            type: String,
            default: ''
        },
    }],
    InterviewSchedules: [{
        CompanyName: {
            type: String,
            default: ''
        },
        InterviewDate: {
            type: Date,
            default: ''
        },
        InterviewTime: {
            type: String,
            default: ''
        },
        InterviewLocation: {
            type: String,
            default: ''
        },
    }],
});

export default Reminders;