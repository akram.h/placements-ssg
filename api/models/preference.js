import mongoose from "mongoose";

const Preferences = new mongoose.Schema({
    CompanyWise: {
        CompanyName: {
            type: String,
            default: '',
        },
        PreferredLocations: [{
            type: String,
            default: '',
        }],
        PreferredIndustries: [{
            type: String,
            default: '',
        }],
        PreferredRoles: [{
            type: String,
            default: '',
        }],
    },
    RoleWise: {
        RoleName: {
            type: String,
            default: '',
        },
        PreferredCompanies: [{
            type: String,
            default: '',
        }],
        PreferredIndustries: [{
            type: String,
            default: '',
        }],
        PreferredLocations: [{
            type: String,
            default: '',
        }],
    },
});

export default Preferences;