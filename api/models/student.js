import mongoose from 'mongoose';
import Reminders from './reminder.js';
import Preferences from './preference.js';

const Students = new mongoose.Schema({
    Name: {
        type: String,
        required: true,
        default: '',
    },
    Qualification: {
        type: String,
        required: true,
        default: '',
    },
    Preferredindustry: {
        type: String,
        default: '',
    },
    Preferredrole: {
        type: String,
        default: '',
    },
    Preferredcompany1: {
        type: String,
        default: '',
    },
    Preferredcompany2: {
        type: String,
        default: '',
    },
    Preferredcompany3: {
        type: String,
        default: '',
    },
    Expectedsalary: {
        type: String,
        default: '',
    },
    Preferredlocation1: {
        type: String,
        default: '',
    },
    Preferredlocation2: {
        type: String,
        default: '',
    },
    Preferredlocation3: {
        type: String,
        default: '',
    },
    Cgpa: {
        type: String,
        required: true,
        default: '',
    },
    Gender: {
        type: String,
        required: true,
        default: '',
    },
    Hometown: {
        type: String,
        default: '',
    },
    Reminders: reminderSchema,
    Preferences: preferenceSchema
});

const reminderSchema = new mongoose.Schema({
    UpcomingEvents: [{
        EventName: {
            type: String,
            default: ''
        },
        EventDate: {
            type: Date,
            default: ''
        },
        EventTime: {
            type: String,
            default: ''
        },
        EventLocation: {
            type: String,
            default: ''
        },
    }],
    ApplicationDeadlines: [{
        JobTitle: {
            type: String,
            default: ''
        },
        CompanyName: {
            type: String,
            default: ''
        },
        DeadlineDate: {
            type: Date,
            default: ''
        },
        ApplicationLink: {
            type: String,
            default: ''
        },
    }],
    InterviewSchedules: [{
        CompanyName: {
            type: String,
            default: ''
        },
        InterviewDate: {
            type: Date,
            default: ''
        },
        InterviewTime: {
            type: String,
            default: ''
        },
        InterviewLocation: {
            type: String,
            default: ''
        },
    }],
});

const preferenceSchema = new mongoose.Schema({
    CompanyWise: {
        CompanyName: {
            type: String,
            default: '',
        },
        PreferredLocations: [{
            type: String,
            default: '',
        }],
        PreferredIndustries: [{
            type: String,
            default: '',
        }],
        PreferredRoles: [{
            type: String,
            default: '',
        }],
    },
    RoleWise: {
        RoleName: {
            type: String,
            default: '',
        },
        PreferredCompanies: [{
            type: String,
            default: '',
        }],
        PreferredIndustries: [{
            type: String,
            default: '',
        }],
        PreferredLocations: [{
            type: String,
            default: '',
        }],
    },
});

export default Students;