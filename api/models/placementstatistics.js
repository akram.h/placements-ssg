import mongoose from "mongoose";

const PlacementStatistics = new mongoose.Schema({
    AcademicYear: {
        type: String,
        default: '',
    },
    TotalStudents: {
        type: Number,
        default: 0,
    },
    StudentsPlaced: {
        type: Number,
        default: 0,
    },
    PlacementPercentage: {
        type: Number,
        default: 0,
    },
    AverageSalary: {
        type: Number,
        default: 0,
    },
    HighestSalary: {
        type: Number,
        default: 0,
    },
    LowestSalary: {
        type: Number,
        default: 0,
    },
    TopRecruitingCompanies: [topRecruitingCompanySchema],
    PlacementDetails: [placementDetailSchema],
});

const topRecruitingCompanySchema = new mongoose.Schema({
    CompanyName: {
        type: String,
        default: '',
    },
    PlacementCount: {
        type: Number,
        default: 0,
    },
});

const placementDetailSchema = new mongoose.Schema({
    StudentName: {
        type: String,
        default: '',
    },
    PlacementCompany: {
        type: String,
        default: '',
    },
    SalaryOffered: {
        type: Number,
        default: 0,
    },
});

export default PlacementStatistics;