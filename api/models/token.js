import mongoose from "mongoose";

const tokenSchema = new mongoose.Schema({
    UserId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users',
        required: true,
    },
    AccessToken: {
        type: String,
        required: true,
    },
    RefreshToken: {
        type: String,
        required: true,
    },
    ExpiresAt: {
        type: Date,
        required: true,
    },
});

const Token = mongoose.model('Tokens', tokenSchema);

export default Token;
