import mongoose from "mongoose";
import Students from "./student";

const BatchDetails = new mongoose.Schema({
    Year: {
        type: String,
        default: ''
    },
    Streams: [
        {
            Name: {
                type: String,
                default: ''
            },
            Students: [Students]
        }
    ]
});

export default BatchDetails;