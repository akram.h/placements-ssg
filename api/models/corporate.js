import mongoose from "mongoose";

const Corporates = new mongoose.Schema({
    Name: {
        type: String,
        default: '',
    },
    Industry: {
        type: String,
        default: '',
    },
    Description: {
        type: String,
        default: '',
    },
    ContactPerson: {
        type: String,
        default: '',
    },
    ContactEmail: {
        type: String,
        default: '',
    },
    ContactPhone: {
        type: String,
        default: '',
    },
    Location: {
        type: String,
        default: '',
    },
    Hq: {
        type: String,
        default: '',
    },
    TotalEmployees: {
        type: Number,
        default: 0,
    },
    HeadHR: {
        type: String,
        default: '',
    },
    Ceo: {
        type: String,
        default: '',
    },
    Website: {
        type: String,
        default: '',
    },
    Products: [
        {
            ProductName: {
                type: String,
                default: '',
            },
            Description: {
                type: String,
                default: '',
            },
        },
    ],
    Services: [
        {
            ServiceName: {
                type: String,
                default: '',
            },
            Description: {
                type: String,
                default: '',
            },
        },
    ],
    SelectionProcess: {
        Qualification: {
            type: String,
            default: '',
        },
        BranchElectives: {
            type: String,
            default: '',
        },
        Cgpa: {
            type: Number,
            default: '',
        },
        OnlineTechnicalTest: {
            type: String,
            default: '',
        },
        OfflineTechnicalTest: {
            type: String,
            default: '',
        },
        TechnicalInterviews: [{
            type: String,
            default: '',
        }],
        Gd: {
            type: String,
            default: '',
        },
        HrInterview: {
            type: String,
            default: '',
        },
        Others: {
            type: String,
            default: '',
        },
    },
    CompanyDetails: {
        OrgStructure: {
            type: String,
            default: '',
        },
        PerformanceCycle: {
            type: String,
            default: '',
        },
    },
    HiringIntent: [hiringIntentSchema],
});

const hiringIntentSchema = new mongoose.Schema({
    JobDescription: jobDescriptionSchema,
    CandidatesAttended: [candidateSchema],
    CandidatesHired: [offerDetailsSchema],
});

const jobDescriptionSchema = new mongoose.Schema({
    Designation: {
        type: String,
        default: '',
    },
    Probation: {
        type: String,
        default: '',
    },
    SalaryFixed: {
        type: Number,
        default: 0,
    },
    SalaryVariable: {
        type: Number,
        default: 0,
    },
    JoiningBonus: {
        type: String,
        default: 0,
    },
    InsuranceMedical: {
        type: String,
        default: '',
    },
    InsuranceLife: {
        type: String,
        default: '',
    },
    OtherBenefits: [{
        type: String,
        default: '',
    }],
    Ctc: {
        type: Number,
        default: 0,
    },
    ExpectedJoiningDate: {
        type: Date,
        default: '',
    },
    OfficeLocation: {
        type: String,
        default: '',
    },
    OnJobTrainingRequired: {
        type: Boolean,
        default: false,
    },
    SpecifyTraining: {
        type: String,
        default: '',
    },
    JobLocation: {
        type: String,
        default: '',
    },
    JobDescriptionText: {
        type: String,
        default: '',
    },
});

const candidateSchema = new mongoose.Schema({
    CandidateName: {
        type: String,
        default: '',
    },
    CandidateEmail: {
        type: String,
        default: '',
    },
    CandidatePhoneNumber: {
        type: String,
        default: '',
    },
    ResumeLink: {
        type: String,
        default: '',
    },
});

const offerDetailsSchema = new mongoose.Schema({
    OfferId: {
        type: String,
        default: '',
    },
    OfferDate: {
        type: Date,
        default: '',
    },
    SalaryOffered: {
        type: Number,
        default: 0,
    },
    JoiningDate: {
        type: Date,
        default: '',
    },
});

export default Corporates;