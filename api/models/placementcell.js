import mongoose from "mongoose";

const PlacementCell = new mongoose.Schema({
    Coordinator: {
        type: String,
        default: '',
    },
    ContactInformation: contactInformationSchema,
    Objective: {
        type: String,
        default: '',
    },
    PlacementTeam: [
        {
            Name: {
                type: String,
                default: '',
            },
            Role: {
                type: String,
                default: '',
            },
            Contact: contactSchema,
        },
    ],
    PlacementCalendar: [eventSchema],
    CorporateRelations: [relationshipDetailsSchema],
    PlacementProcess: {
        type: String,
        default: '',
    },
});

const contactInformationSchema = new mongoose.Schema({
    PhoneNumber: {
        type: String,
        default: '',
    },
    EmailAddress: {
        type: String,
        default: '',
    },
});

const contactSchema = new mongoose.Schema({
    PhoneNumber: {
        type: String,
        default: '',
    },
    Email: {
        type: String,
        default: '',
    },
});

const eventSchema = new mongoose.Schema({
    Event: {
        type: String,
        default: '',
    },
    Date: {
        type: Date,
        default: '',
    },
    Location: {
        type: String,
        default: '',
    },
});

const relationshipDetailsSchema = new mongoose.Schema({
    CompanyName: {
        type: String,
        default: '',
    },
    ContactPerson: {
        type: String,
        default: '',
    },
    ContactEmail: {
        type: String,
        default: '',
    },
    RelationshipDetails: {
        type: String,
        default: '',
    },
});

export default PlacementCell;