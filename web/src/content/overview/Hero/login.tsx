import React, { useState } from 'react';
import {
  Stack,
  TextField,
  InputAdornment,
  IconButton,
  Checkbox,
  Link,
  FormControlLabel,
  Button
} from '@mui/material';
import VisibilityOffTwoToneIcon from '@mui/icons-material/VisibilityOffTwoTone';
import RemoveRedEyeTwoToneIcon from '@mui/icons-material/RemoveRedEyeTwoTone';
import { Link as RouterLink } from 'react-router-dom';

interface LoginProps {
  handleClick: () => void;
}

const LoginForm: React.FC<LoginProps> = ({ handleClick }) => {
  const [showPassword, setShowPassword] = useState(false);

  return (
    <>
      <Stack spacing={3}>
        <TextField name="email" label="Email address" />

        <TextField
          name="password"
          label="Password"
          type={showPassword ? 'text' : 'password'}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
               <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                  {showPassword ? (
                    <VisibilityOffTwoToneIcon />
                  ) : (
                    <RemoveRedEyeTwoToneIcon />
                  )}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Stack>

      <Stack direction="row" alignItems="center" justifyContent="space-between" sx={{ my: 2 }}>
      <FormControlLabel
          control={<Checkbox name="remember" />}
          label="Remember me"
        />        <Link variant="subtitle2" underline="hover">
          Forgot password?
        </Link>
      </Stack>


      <Button component={RouterLink}
            to="/dashboards/crypto"
            size="large"
            variant="contained" fullWidth  type="submit" onClick={handleClick}>
        Login
      </Button>
    </>
  );
};

export default LoginForm;
