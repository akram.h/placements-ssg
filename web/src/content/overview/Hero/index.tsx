import React from 'react';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import LoginForm from '../Hero/login';
import styled from '@emotion/styled';

function Hero() {
  const handleLogin = () => {
    console.log('login clicked');
  };

  const gridStyle = {
    padding: '15px 15px',
    boxShadow: '0px 0px 15px rgba(0, 0, 0, 0.2)', // Add a shadow to the right side
    borderRadius: '10px', // Add rounded corners if needed
  };

  return (
    <Container maxWidth="lg" sx={{ textAlign: 'center', py: 5 }}>
      <Grid container justifyContent="center" alignItems="center" spacing={0}>
        <Grid item xs={12} md={6} >
          <Typography variant="h3" sx={{ color: 'black', mb: 2 }}>
            Hi, Welcome Back
          </Typography>
          <img
            src="/illustrations/illustration_login.png"
            alt="login"
            style={{ width: '100%', maxWidth: '300px', borderRadius: '10px' }}
          />
        </Grid>
        <Grid item xs={12} md={6} sx={gridStyle}>
          <LoginForm handleClick={handleLogin} />
        </Grid>
      </Grid>
    </Container>
  );
}

export default Hero;
