import { Helmet } from "react-helmet-async";
import PageTitleWrapper from "../../../../components/PageTitleWrapper";
import PageTitle from "../../../../components/PageTitle";

function Resources(){
    return(
        <>
        <Helmet>
        <title>Tooltips - Components</title>
      </Helmet>
      <PageTitleWrapper>
        <PageTitle
          heading="Resources"
          subHeading="You can access, manage, and update your resources in one place."
          docs="https://material-ui.com/components/tooltips/"
        />
      </PageTitleWrapper>
        
        </>
    )
}

export default Resources;